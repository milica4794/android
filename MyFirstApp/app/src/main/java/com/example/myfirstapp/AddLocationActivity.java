package com.example.myfirstapp;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfirstapp.interfaces.GetDataService;
import com.example.myfirstapp.models.Landmark;
import com.example.myfirstapp.models.User;
import com.example.myfirstapp.network.RetrofitInstance;
import com.example.myfirstapp.network.SingleShotLocationProvider;
import com.example.myfirstapp.tools.Util;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_CONTACTS;

public class AddLocationActivity extends FragmentActivity
        implements OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private static final int PICK_IMAGE = 100;
    private EditText mName;
    private EditText mDescription;
    private ImageView imageView;
    private MapFragment mMapFragment;
    private GoogleMap mMap;
    private ConstraintLayout constraintLayout;
    private Marker mMarker;
    private SingleShotLocationProvider.GPSCoordinates mLocation;
    private ScrollView mScrollView;
    private static final int REQUEST_ACCESS_FINE_LOCATION = 0;
    private LatLng newPosition;
    //add for Image object

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location);

        constraintLayout = (ConstraintLayout) findViewById(R.id.profile_layout);
        mName = (EditText) findViewById(R.id.new_name);
        mDescription = (EditText) findViewById(R.id.new_description);
        imageView = (ImageView) findViewById(R.id.imageViewLoc);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
            }
        });
        Button mEmailSignInButton = (Button) findViewById(R.id.save_location_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addLocation();
            }
        });
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void addLocation(){
        String name = mName.getText().toString();
        String description = mDescription.getText().toString();
        Bitmap userImage =((BitmapDrawable)imageView.getDrawable()).getBitmap();
     //   ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
     //   userImage.compress(Bitmap.CompressFormat.JPEG, 20, outputStream);
     //   Bitmap compressedBitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(outputStream.toByteArray()));
        Bitmap resized = Bitmap.createScaledBitmap(userImage, 120,120, false);
        byte[] image = Util.getBitmapAsByteArray(resized);

        Intent in = getIntent();




    User user = new User();
    user.setUsername(in.getStringExtra("username"));

        Landmark landmark = new Landmark();
        landmark.setName(name);
        landmark.setDescription(description);
        landmark.setLatitude(newPosition.latitude);
        landmark.setLongitude(newPosition.longitude);
        landmark.setPhoto(Base64.encodeToString(image, Base64.DEFAULT));
        landmark.setAddedBy(user);
     //   landmark.setLatitude(45.267136);
     //   landmark.setLongitude(19.833549);

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
         //   addresses = geocoder.getFromLocation(45.267136, 19.833549,1);
            addresses = geocoder.getFromLocation(mMarker.getPosition().latitude, mMarker.getPosition().longitude, 1);

            landmark.setCity(addresses.get(0).getLocality().toString());
            landmark.setCountry(addresses.get(0).getCountryName().toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
        HashMap<String, Object> lista = new HashMap<String, Object>();
        lista.put("landmark",landmark);
        lista.put("user", user);
        GetDataService service = RetrofitInstance.getRetrofitInstance().create(GetDataService.class);
        Call<Landmark> call = service.addLandmark(lista);

        call.enqueue(new Callback<Landmark>() {
            @Override
            public void onResponse(Call<Landmark> call, Response<Landmark> response) {
                Log.i("service", "saving landmark");

                if(response.body()==null)
                    Toast.makeText(getApplicationContext(), "NOPE", Toast.LENGTH_SHORT).show();
                else {

                    Toast.makeText(getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Landmark> call, Throwable t) {
            }
        });

        Intent data = new Intent();
        data.putExtra("newLocName", name);
        data.putExtra("newLocDescription", description);
        // Activity finished ok, return the data
        setResult(RESULT_OK, data);
        finish();
    }

    private void openGallery(){
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        startActivityForResult(chooserIntent, PICK_IMAGE);
        /*Intent gallery =
                new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);*/
    }

    //catch when user changed image
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            Uri imageUri = data.getData();
            imageView.setImageURI(imageUri);
        }
    }

    @Override
    public void onMapReady(final GoogleMap map) {

        SingleShotLocationProvider.requestSingleUpdate(map,AddLocationActivity.this, new SingleShotLocationProvider.LocationCallback() {
            @Override
            public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                Log.d("Location", "my Location is " + location.toString());
                mLocation = location;
                LatLng sydney = new LatLng(mLocation.latitude, mLocation.longitude);
                mMarker = map.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney").draggable(true));
                map.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                map.getUiSettings().setScrollGesturesEnabled(true);
                map.setOnMapClickListener(AddLocationActivity.this);
                mMap = map;
                setLocation();
            }
        });

    }

    private void setLocation() {
        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            return;
        }
        else {
            Snackbar.make(constraintLayout, R.string.permission_map_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{ACCESS_FINE_LOCATION}, REQUEST_ACCESS_FINE_LOCATION);
                        }
                    }).show();
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setLocation();
            }
        }
    }



    @Override
    public void onMapClick(LatLng latLng) {
        newPosition = mMarker.getPosition();
        mMarker.setPosition(latLng);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
    }
}
