package com.example.myfirstapp.fragments;

import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfirstapp.DetailsActivity;

import com.example.myfirstapp.FavouritesModel;

import com.example.myfirstapp.MainActivity;
import com.example.myfirstapp.R;
import com.example.myfirstapp.WonderModel;
import com.example.myfirstapp.interfaces.GetDataService;
import com.example.myfirstapp.models.Landmark;
import com.example.myfirstapp.network.RetrofitInstance;

import java.util.ArrayList;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouritesFragment extends Fragment {

    ArrayList<FavouritesModel> listitems = new ArrayList<>();
    RecyclerView MyRecyclerView;
    ArrayList<String> Wonders = new ArrayList<>();
    ArrayList<String> Images = new ArrayList<>();
    ArrayList<String> Ids = new ArrayList<>();
    SetFavouriteTask favTask;
    Long usID;
    //String Wonders[] = {"Chichen Itza","Colosseum","Petra","Great Wall of China","Machu Picchu","Christ the Redeemer","Taj Mahal"};
    //{R.drawable.ic_person,R.drawable.ic_person,R.drawable.ic_person,R.drawable.ic_person,R.drawable.ic_person,R.drawable.ic_person,R.drawable.ic_person};

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Wonders = getArguments().getStringArrayList("user wonders");
        Images = getArguments().getStringArrayList("images");
        usID = Long.parseLong(getArguments().getString("userID"));
        Ids = getArguments().getStringArrayList("ids");
        Log.i("Images size: ", Integer.toString(Images.size()));
        initializeList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_card, container, false);
        MyRecyclerView = (RecyclerView) view.findViewById(R.id.cardView);
        MyRecyclerView.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(getActivity());
        MyLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        if (listitems.size() > 0 & MyRecyclerView != null) {
            MyRecyclerView.setAdapter(new FavouritesFragment.MyAdapter(listitems));
        }
        MyRecyclerView.setLayoutManager(MyLayoutManager);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public class MyAdapter extends RecyclerView.Adapter<FavouritesFragment.MyViewHolder> {
        private ArrayList<FavouritesModel> list;
        private String landmarkID;

        public MyAdapter(ArrayList<FavouritesModel> Data) {
            list = Data;
        }

        @Override
        public FavouritesFragment.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recycle_items, parent, false);
            FavouritesFragment.MyViewHolder holder = new FavouritesFragment.MyViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(final FavouritesFragment.MyViewHolder holder, int position) {

            holder.titleTextView.setText(list.get(position).getCardName());
            holder.image = list.get(position).getImageResourceId();
            byte[] imgByte = Base64.decode(list.get(position).getImageResourceId(), Base64.DEFAULT);
            if(imgByte!=null){
                holder.coverImageView.setImageBitmap(BitmapFactory.decodeByteArray(imgByte,0,imgByte.length));
            }

            //holder.coverImageView.setTag(list.get(position).getImageResourceId());
            holder.likeImageView.setTag(R.drawable.ic_favorite_full);
            holder.likeImageView.setImageResource(R.drawable.ic_favorite_full);
            holder.titleTextView.setTag(Ids.get(position));
            holder.landmarkID =Ids.get(position);
            landmarkID = Ids.get(position);
            holder.likeImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                   int id = (int)holder.likeImageView.getTag();
                    if( id == R.drawable.ic_favorite_empty){

                        holder.likeImageView.setTag(R.drawable.ic_favorite_full);
                        holder.likeImageView.setImageResource(R.drawable.ic_favorite_full);

                        favTask = new SetFavouriteTask(usID, Long.parseLong(landmarkID), true);
                        favTask.execute((Void) null);


                        Toast.makeText(getActivity(),holder.titleTextView.getText()+" added to favourites",Toast.LENGTH_SHORT).show();

                    }else{

                        holder.likeImageView.setTag(R.drawable.ic_favorite_empty);
                        holder.likeImageView.setImageResource(R.drawable.ic_favorite_empty);

                        favTask = new SetFavouriteTask(usID, Long.parseLong(landmarkID), false);
                        favTask.execute((Void) null);

                        Toast.makeText(getActivity(),holder.titleTextView.getText()+" removed from favourites",Toast.LENGTH_SHORT).show();


                    }

                }
            });

        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView titleTextView;
        public ImageView coverImageView;
        public ImageView likeImageView;
        public ImageView shareImageView;
        public String landmarkID;
        public String image;

        public MyViewHolder(View v) {
            super(v);
            titleTextView = (TextView) v.findViewById(R.id.titleTextView);
            coverImageView = (ImageView) v.findViewById(R.id.coverImageView);
            likeImageView = (ImageView) v.findViewById(R.id.likeImageView);
            shareImageView = (ImageView) v.findViewById(R.id.shareImageView);
            likeImageView.setTag(R.drawable.ic_favorite_full);
            likeImageView.setImageResource(R.drawable.ic_favorite_full);
            landmarkID = (String)titleTextView.getTag();
            likeImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    int id = (int)likeImageView.getTag();
                    if( id == R.drawable.ic_favorite_empty){

                        likeImageView.setTag(R.drawable.ic_favorite_full);
                        likeImageView.setImageResource(R.drawable.ic_favorite_full);

                        favTask = new SetFavouriteTask(usID, Long.parseLong(landmarkID), true);
                        favTask.execute((Void) null);

                        Toast.makeText(getActivity(),titleTextView.getText()+" added to favourites",Toast.LENGTH_SHORT).show();

                    }else{

                        likeImageView.setTag(R.drawable.ic_favorite_empty);
                        likeImageView.setImageResource(R.drawable.ic_favorite_empty);
                        favTask = new SetFavouriteTask(usID, Long.parseLong(landmarkID), false);
                        favTask.execute((Void) null);
                        Toast.makeText(getActivity(),titleTextView.getText()+" removed from favourites",Toast.LENGTH_SHORT).show();


                    }

                }
            });



            shareImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Uri imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                            "://" + getResources().getResourcePackageName(coverImageView.getId())
                            + '/' + "drawable" + '/' + getResources().getResourceEntryName((int)coverImageView.getTag()));


                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.putExtra(Intent.EXTRA_STREAM,imageUri);
                    shareIntent.setType("image/jpeg");
                    startActivity(Intent.createChooser(shareIntent, getResources().getText(1)));

                }
            });

            //TODO:detaljniji prikaz lokacije
            coverImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent detailsIntent = new Intent(getContext(), DetailsActivity.class);
                    detailsIntent.putExtra("location_name", titleTextView.getText().toString());
                    detailsIntent.putExtra("location_id", landmarkID);
                    detailsIntent.putExtra("location_image", image);

                    detailsIntent.putExtra("userID", usID);
                    detailsIntent.putExtra("location_id", landmarkID);
                    startActivity(detailsIntent);

                }
            });

        }
    }

    public void initializeList() {
        listitems.clear();

        for(int i =0;i<Wonders.size();i++){


            FavouritesModel item = new FavouritesModel();
            item.setCardName(Wonders.get(i));
            item.setImageResourceId(Images.get(i));
            item.setIsfav(0);
            item.setIsturned(0);
            listitems.add(item);

        }
    }

    public class SetFavouriteTask extends AsyncTask<Void, Void, Boolean> {

        private long userID;
        private long landmarkId;
        private boolean favourite;

        public SetFavouriteTask(long userID, long landmarkId, boolean favourite) {
            this.userID = userID;
            this.landmarkId = landmarkId;
            this.favourite = favourite;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            GetDataService service = RetrofitInstance.getRetrofitInstance().create(GetDataService.class);

            Call<Landmark> call;
            if(this.favourite == true){
                call = service.addToFavourite(this.userID+";"+this.landmarkId);
            }
            else {
                call = service.removeFromFavourite(this.userID+";"+this.landmarkId);
            }
            call.enqueue(new Callback<Landmark>() {
                @Override
                public void onResponse(Call<Landmark> call, Response<Landmark> response) {

                    if (response.isSuccessful()) {
                        if (response.code() == 200) {

                        }

                    } else {
                    }
                }


                @Override
                public void onFailure(Call<Landmark> call, Throwable t) {
                    Log.i("onFailure", "Usao u onFailure(MainActivity)");

                }
            });

            return false;
        }
    }
}