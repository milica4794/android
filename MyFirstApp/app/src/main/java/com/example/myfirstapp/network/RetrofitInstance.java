package com.example.myfirstapp.network;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import okhttp3.logging.HttpLoggingInterceptor;

public class RetrofitInstance {

    private static Retrofit retrofit;
   // private static final String BASE_URL = "http://192.168.43.80:8080";


    private static final String BASE_URL = "http://192.168.1.103:8080";

   // private static final String BASE_URL = "http://192.168.0.11:8080";
   // private static final String BASE_URL = "http://192.168.1.8:8080";

  //  private static final String BASE_URL = "http://192.168.1.102:8080";



    /**
     * Create an instance of Retrofit object
     * */
    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .client(GetClient())
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static OkHttpClient GetClient(){
        HttpLoggingInterceptor  loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        return httpClient.addInterceptor(loggingInterceptor).build();
    }
}
