package com.example.myfirstapp.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

public class UserDBContentProvider extends ContentProvider{

    private UserSQLiteHelper database;

    //povratni kodovi za putanje
    private static final int USER = 10;
    private static final int USER_ID = 20;
    private static final int USER_EMAIL = 30;
    private static final int USER_IMAGE = 40;

    //za authority se stavlja root aplikacije, po mom shvatanju
    private static final String AUTHORITY = "com.example.myfirstapp";

    private static final String USERS_PATH = "users";

    //putanja u telefonu jer ipak je ovo lokalna baza
    public static final Uri CONTENT_URI_USERS = Uri.parse("content://" + AUTHORITY + "/" + USERS_PATH);

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    //DA SE ODMAH NAPRAVE PUTANJE I KODOVI
    //.addURI(Authority, putanja, kod) - koristi se za proveru putanje i dopune upita u .query metodi
    static {
        sURIMatcher.addURI(AUTHORITY, USERS_PATH, USER);
        sURIMatcher.addURI(AUTHORITY, USERS_PATH + "/email/*", USER_EMAIL);
        sURIMatcher.addURI(AUTHORITY, USERS_PATH + "/image/*", USER_IMAGE);
        sURIMatcher.addURI(AUTHORITY, USERS_PATH + "/#", USER_ID); //# se koristi da bi se prepoznao broj
    }

    @Override
    public boolean onCreate() {

        database = new UserSQLiteHelper(getContext());
        //provera reda radi
        if(database != null)
            return true;
        else
            return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        //Izbegavanje injection napada, koristiti builder
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase db = database.getWritableDatabase();
        switch (uriType) {
            case USER_ID:
                // Adding the ID to the original query
                //NISAM TESTIRALA OVO, MOZDA I RADI
                queryBuilder.appendWhere(UserSQLiteHelper.COLUMN_ID + "="
                        + uri.getLastPathSegment());
                queryBuilder.setTables(UserSQLiteHelper.TABLE_USER);
                break;
            case USER_EMAIL:
                Log.i("Email Query", uri.getLastPathSegment());
                //OVAKO RADI BEZ PROBLEMA, ONO NEW STRING[] JE GDE STAVLJAS PODATKE ZA WHERE KLAUZULU
                Cursor cursor = db.rawQuery("SELECT _id,username, email, password FROM users WHERE email = ?; ", new String[] {uri.getLastPathSegment()});
                return cursor;
                //$FALL-THROUGH$
            case USER_IMAGE:
                Log.i("Email Query", uri.getLastPathSegment());
                //OVAKO RADI BEZ PROBLEMA, ONO NEW STRING[] JE GDE STAVLJAS PODATKE ZA WHERE KLAUZULU
                Cursor cursor2 = db.rawQuery("SELECT image FROM users WHERE email = ?; ", new String[] {uri.getLastPathSegment()});
                return cursor2;

            case USER:
                // Set the table
                //SAMO OVA VERZIJA UPITA ZA QUERYBUILDER RADI WTF????/
                queryBuilder.setTables(UserSQLiteHelper.TABLE_USER);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        Cursor cursor = queryBuilder.query(db, projection, selection,                selectionArgs, null, null, sortOrder);
        // make sure that potential listeners are getting notified
        Log.i("cursor size", Integer.toString(cursor.getCount()));
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        Uri retVal = null;
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        long id = 0;
        switch (uriType) {
            case USER:
                id = sqlDB.insert(UserSQLiteHelper.TABLE_USER, null, values);
                retVal = Uri.parse(USERS_PATH + "/" + id);
                Log.i("insert query result", retVal.getPath());
                break;
            case USER_ID:
                // Adding the ID to the original query
                sqlDB.update(UserSQLiteHelper.TABLE_USER, values, " _id="+uri.getLastPathSegment(), null);
                Log.w("updateUserID", uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return retVal;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        long id = 0;
        int rowsDeleted = 0;
        switch (uriType) {
            case USER:
                rowsDeleted = sqlDB.delete(UserSQLiteHelper.TABLE_USER,
                        selection,
                        selectionArgs);
                break;
            case USER_ID:
                String idUser = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(UserSQLiteHelper.TABLE_USER,
                            UserSQLiteHelper.COLUMN_ID + "=" + idUser,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(UserSQLiteHelper.TABLE_USER,
                            UserSQLiteHelper.COLUMN_ID + "=" + idUser
                                    + " and "
                                    + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        long id = 0;
        int rowsUpdated = 0;
        switch (uriType) {
            case USER:
                rowsUpdated = sqlDB.update(UserSQLiteHelper.TABLE_USER,
                        values,
                        selection,
                        selectionArgs);
                break;
            case USER_ID:
                String idUser = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(UserSQLiteHelper.TABLE_USER,
                            values,
                            UserSQLiteHelper.COLUMN_ID + "=" + idUser,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(UserSQLiteHelper.TABLE_USER,
                            values,
                            UserSQLiteHelper.COLUMN_ID + "=" + idUser
                                    + " and "
                                    + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }
}
