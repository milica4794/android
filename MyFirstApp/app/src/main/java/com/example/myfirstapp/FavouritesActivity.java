package com.example.myfirstapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.myfirstapp.fragments.FavouritesFragment;
import com.example.myfirstapp.interfaces.GetDataService;
import com.example.myfirstapp.models.Landmark;
import com.example.myfirstapp.network.RetrofitInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouritesActivity extends AppCompatActivity {

    private String usEmail;
    private String usPassword;
    private String usUsername;
    private long usID;
    private static final int FAVES_OK = 500;
    private GetFavouritesTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);
        Intent intent = getIntent();
        usEmail = intent.getStringExtra("email");
        usPassword = intent.getStringExtra("password");
        usUsername = intent.getStringExtra("username");
        usID = intent.getLongExtra("userId", -1);


        task = new GetFavouritesTask(usID);
        task.execute((Void) null);
        //setNavImage();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent stuff = new Intent();
                stuff.putExtra("email", usEmail);
                stuff.putExtra("password", usPassword);
                stuff.putExtra("username", usUsername);
                stuff.putExtra("userId", usID);
                setResult(FAVES_OK, stuff);
                finish();
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {

        Intent stuff = new Intent();
        stuff.putExtra("email", usEmail);
        stuff.putExtra("password", usPassword);
        stuff.putExtra("username", usUsername);
        stuff.putExtra("userId", usID);
        setResult(FAVES_OK, stuff);
        super.onBackPressed();
    }

    public class GetFavouritesTask extends AsyncTask<Void,Void, Boolean> {

        private final long usId;


        public GetFavouritesTask(long userID) {
           this.usId = userID;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            GetDataService service = RetrofitInstance.getRetrofitInstance().create(GetDataService.class);
            Call<List<Landmark>> call = service.getFavourites(usId);

            call.enqueue(new Callback<List<Landmark>>() {
                @Override
                public void onResponse(Call<List<Landmark>> call, Response<List<Landmark>> response) {
                    //   Log.i("onResponse", "Usao u onResponse(LoginActivity)");

                    if (response.isSuccessful())
                    {
                        if(response.code()==200) {
                            FragmentManager fm = getSupportFragmentManager();
                            Fragment fragment = fm.findFragmentById(R.id.favouritesFragmentContainer);


                            Log.i("ResponseMessage","Fetched favourites");
                            Log.i("ResponseBody",Integer.toString(response.body().size()));
                            ArrayList<Landmark> favourites = (ArrayList<Landmark>)response.body();
                            ArrayList<String> title = new ArrayList<String>();
                            ArrayList<String> imgs = new ArrayList<>();
                            ArrayList<String> ids = new ArrayList<>();
                            ArrayList<String> id = new ArrayList<>();
                            id.add(String.valueOf(usID));
                            for(int i = 0; i < favourites.size(); i++){
                                title.add(favourites.get(i).getName());
                                imgs.add(favourites.get(i).getPhoto());
                                ids.add(String.valueOf(favourites.get(i).getId()));
                            }

                            if (fragment == null) {
                                fragment = new FavouritesFragment();
                                Bundle args = new Bundle();

                                args.putString("userID", Long.toString(usID));
                                args.putStringArrayList("user wonders", title);
                                args.putStringArrayList("images", imgs);
                                args.putStringArrayList("ids", ids);
                                fragment.setArguments(args);
                                ;
                                fm.beginTransaction()
                                        .add(R.id.favouritesFragmentContainer, fragment)
                                        .commit();
                            }
                        }

                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

                        //  mPasswordView.setError(getString(R.string.error_incorrect_password));
                        // mPasswordView.requestFocus();
                    }
                }


                @Override
                public void onFailure(Call<List<Landmark>> call, Throwable t) {
                    Log.i("onFailure", "Usao u onFailure(FavouritesActivity)");

                }
            });

            return false;
        }
    }
}
