package com.example.myfirstapp.initialisers;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.example.myfirstapp.db.UserDBContentProvider;
import com.example.myfirstapp.db.UserSQLiteHelper;

public class DBUtil {
    public static void initDB(Activity activity) {
        UserSQLiteHelper dbHelper = new UserSQLiteHelper(activity);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        {

            ContentValues entry = new ContentValues();
            entry.put(UserSQLiteHelper.COLUMN_USERNAME, "test");
            entry.put(UserSQLiteHelper.COLUMN_EMAIL, "nekimejl@mejlovi.ts");
            entry.put(UserSQLiteHelper.COLUMN_PASSWORD, "test");
            activity.getContentResolver().insert(UserDBContentProvider.CONTENT_URI_USERS, entry);

            entry = new ContentValues();
            entry.put(UserSQLiteHelper.COLUMN_USERNAME, "sauron");
            entry.put(UserSQLiteHelper.COLUMN_EMAIL, "lordoftheonering@mrdmail.mdde");
            entry.put(UserSQLiteHelper.COLUMN_PASSWORD, "ring");

            activity.getContentResolver().insert(UserDBContentProvider.CONTENT_URI_USERS, entry);
        }

        db.close();
    }
}
