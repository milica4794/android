package com.example.myfirstapp;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfirstapp.db.UserDBContentProvider;
import com.example.myfirstapp.db.UserSQLiteHelper;
import com.example.myfirstapp.interfaces.GetDataService;
import com.example.myfirstapp.models.User;
import com.example.myfirstapp.network.RetrofitInstance;

import java.io.ByteArrayOutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    private static final int PICK_IMAGE = 100;
    private static final int PROFILE_SET = 200;
    private ImageView imageView;
    private TextView txtEmail;
    private TextView txtPaassword;
    private TextView txtUsername;
    private Bitmap userImage;
    private long ID;
    private ChangeProfileTask changeProf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_details);

        Intent intent = getIntent();
        String usEmail = intent.getStringExtra("email");
        String usPassword = intent.getStringExtra("password");
        String usUsername = intent.getStringExtra("username");
        String image = intent.getStringExtra("userImage");
        ID = intent.getLongExtra("userId", -1);
        txtEmail = findViewById(R.id.user_email);
        txtEmail.setText(usEmail);
        txtUsername = findViewById(R.id.new_username);
        txtPaassword = findViewById(R.id.new_password);
        txtUsername.setText(usUsername);
        //dobavljanje slike
       // Uri queryUri = Uri.parse(UserDBContentProvider.CONTENT_URI_USERS + "/image/" + usEmail);
        imageView = (ImageView) findViewById(R.id.imageViewUser);
      /*  String[] imageColumn = {UserSQLiteHelper.COLUMN_IMAGE};
        Cursor cursor = getContentResolver().query(queryUri, imageColumn, null, null,
                null);
        cursor.moveToFirst();
        byte[] imgByte = cursor.getBlob(0);
        cursor.close();*/
      byte[] imgByte = Base64.decode(image, Base64.DEFAULT);
        if(imgByte != null){
            userImage = BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length);
            imageView.setImageBitmap(userImage);
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        Button saveChanges = (Button) findViewById(R.id.save_user_changes_button);
        saveChanges.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View view) {
                                               saveChanges();
                                           }
        });

        /*// Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        // Capture the layout's TextView and set the string as its text
        TextView textView = findViewById(R.id.editText2);
        textView.setText(message);*/
    }

    public void saveChanges() {
        userImage=((BitmapDrawable)imageView.getDrawable()).getBitmap();
        byte[] data = getBitmapAsByteArray(userImage);
        String userImage = Base64.encodeToString(data, Base64.DEFAULT);
        Log.i("saveImageProf", "no to save user profile changes");
      /*  ContentValues entry = new ContentValues();
        entry.put(UserSQLiteHelper.COLUMN_USERNAME, txtUsername.getText().toString());
        entry.put(UserSQLiteHelper.COLUMN_EMAIL, txtEmail.getText().toString());
        entry.put(UserSQLiteHelper.COLUMN_PASSWORD, txtPaassword.getText().toString());
        entry.put(UserSQLiteHelper.COLUMN_IMAGE, data);
        Uri query = Uri.parse(UserDBContentProvider.CONTENT_URI_USERS + "/" + ID);
        getContentResolver().insert(query, entry);
*/
      changeProf = new ChangeProfileTask(txtEmail.getText().toString(),txtPaassword.getText().toString(), txtUsername.getText().toString(), ID, userImage);
      changeProf.execute((Void) null);
        Intent stuff = new Intent();
        Intent oldStuff = getIntent();
        stuff.putExtra("email", txtEmail.getText().toString());
        stuff.putExtra("password", txtPaassword.getText().toString());
        stuff.putExtra("username", txtUsername.getText().toString());
        stuff.putExtra("userImage",userImage);
        stuff.putExtra("userId", ID);
        oldStuff.putExtra("userImage", userImage);
        setResult(PROFILE_SET, stuff);
        finish();
    }

    //CHANGE USER IMAGE
    //open gallery
    private void openGallery(){
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        startActivityForResult(chooserIntent, PICK_IMAGE);
        /*Intent gallery =
                new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);*/
    }

    //catch when user changed image
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            Uri imageUri = data.getData();
            imageView.setImageURI(imageUri);
        }
    }

    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        Log.i("profilebitmap", "fixing bitmap to strea");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }

    public class ChangeProfileTask extends AsyncTask<Void,Void, Boolean>{

        private final String mEmail;
        private final String mPassword;
        private String mUsername;
        private long userID;
        private String mImage;

        public ChangeProfileTask(String mEmail, String mPassword,String mUsername, long userID, String mImage) {
            this.mEmail = mEmail;
            this.mPassword = mPassword;
            this.mUsername = mUsername;
            this.userID = userID;
            this.mImage = mImage;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            GetDataService service = RetrofitInstance.getRetrofitInstance().create(GetDataService.class);

            User user = new User();
            user.setEmail(mEmail);
            user.setPassword(mPassword);
            user.setUsername(mUsername);
            user.setId(userID);
            user.setPicture(mImage);
            Call<User> call = service.updateUser(user);

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    //   Log.i("onResponse", "Usao u onResponse(LoginActivity)");
                    //no user

                    if (response.isSuccessful())
                    {
                        if(response.code()==200) {
                            Log.i("ResponseMessage","Users profile changed");

                        }

                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

                        //  mPasswordView.setError(getString(R.string.error_incorrect_password));
                        // mPasswordView.requestFocus();
                    }
                }


                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Log.i("onFailure", "Usao u onFailure(ProfileActivity)");

                }
            });

            return false;
        }
    }
}
