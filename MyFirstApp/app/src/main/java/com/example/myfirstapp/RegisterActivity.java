package com.example.myfirstapp;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.myfirstapp.db.UserDBContentProvider;
import com.example.myfirstapp.db.UserSQLiteHelper;
import com.example.myfirstapp.interfaces.GetDataService;
import com.example.myfirstapp.models.User;
import com.example.myfirstapp.network.RetrofitInstance;
import com.example.myfirstapp.tools.Util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity  extends AppCompatActivity {

    private static final int PICK_IMAGE = 100;
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private EditText mUsernameView;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        mEmailView = (AutoCompleteTextView) findViewById(R.id.register_email);
        mPasswordView = (EditText) findViewById(R.id.register_password);
        mUsernameView = (EditText) findViewById(R.id.register_username);
        imageView = (ImageView)findViewById(R.id.imageViewNewUser);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });
        Button mEmailSignInButton = (Button) findViewById(R.id.register_done_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });
    }

    private void registerUser(){


        /*
                    public boolean compress (Bitmap.CompressFormat format, int quality, OutputStream stream)
                        Write a compressed version of the bitmap to the specified outputstream.
                        If this returns true, the bitmap can be reconstructed by passing a
                        corresponding inputstream to BitmapFactory.decodeStream().

                        Note: not all Formats support all bitmap configs directly, so it is possible
                        that the returned bitmap from BitmapFactory could be in a different bitdepth,
                        and/or may have lost per-pixel alpha (e.g. JPEG only supports opaque pixels).

                        Parameters
                        format : The format of the compressed image
                        quality : Hint to the compressor, 0-100. 0 meaning compress for small size,
                            100 meaning compress for max quality. Some formats,
                            like PNG which is lossless, will ignore the quality setting
                        stream: The outputstream to write the compressed data.

                        Returns
                            true if successfully compressed to the specified stream.
                */

                /*
                    Bitmap.CompressFormat
                        Specifies the known formats a bitmap can be compressed into.

                            Bitmap.CompressFormat  JPEG
                            Bitmap.CompressFormat  PNG
                            Bitmap.CompressFormat  WEBP
Bitmap bmp = intent.getExtras().get("data");
ByteArrayOutputStream stream = new ByteArrayOutputStream();
bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
              ***********    Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray,0,byteArray.length);
                */
        // Compress the bitmap with JPEG format and quality 50%
      //  userImage.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
     //   byte[] data = outputStream.toByteArray();
        Bitmap userImage =((BitmapDrawable)imageView.getDrawable()).getBitmap();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        userImage.compress(Bitmap.CompressFormat.JPEG, 20, outputStream);
        Bitmap compressedBitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(outputStream.toByteArray()));
        Bitmap resized = Bitmap.createScaledBitmap(compressedBitmap, 120,120, false);
        byte[] data = Util.getBitmapAsByteArray(resized);



        Log.i("saveImageProf", "no to save user profile changes");
        /*
        ContentValues entry = new ContentValues();
        entry.put(UserSQLiteHelper.COLUMN_USERNAME,  mUsernameView.getText().toString());
        entry.put(UserSQLiteHelper.COLUMN_EMAIL, mEmailView.getText().toString());
        entry.put(UserSQLiteHelper.COLUMN_PASSWORD, mPasswordView.getText().toString());
        entry.put(UserSQLiteHelper.COLUMN_IMAGE, data);

        getContentResolver().insert(UserDBContentProvider.CONTENT_URI_USERS, entry);
        */
        User user = new User();
        user.setPassword(mPasswordView.getText().toString());
        user.setEmail(mEmailView.getText().toString());
        user.setUsername(mUsernameView.getText().toString());

        user.setPicture(Base64.encodeToString(data, Base64.DEFAULT));
        GetDataService service = RetrofitInstance.getRetrofitInstance().create(GetDataService.class);

        Call<User> call = service.addUser(user);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.i("service", "saving user");
                //no user
                if(response.body()==null)
                    Toast.makeText(getApplicationContext(), "NOPE", Toast.LENGTH_SHORT).show();
                else {

                    Toast.makeText(getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
            }
        });

        setResult(RESULT_OK);
        finish();
    }

    private void openGallery(){
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        startActivityForResult(chooserIntent, PICK_IMAGE);
        /*Intent gallery =
                new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);*/
    }

    //catch when user changed image
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            Uri imageUri = data.getData();
            imageView.setImageURI(imageUri);
        }
    }

}
