package com.example.myfirstapp;

import com.example.myfirstapp.models.Landmark;

public class CommentFragmentModel {

    String commentUser;
    String commentText;
    Landmark landmark;

    public String getCommentUser() {
        return commentUser;
    }

    public void setCommentUser(String commentUser) {
        this.commentUser = commentUser;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Landmark getLandmark() {
        return landmark;
    }

    public void setLandmark(Landmark landmark) {
        this.landmark = landmark;
    }

    @Override
    public String toString() {
        return "CommentFragmentModel{" +
                "commentUser='" + commentUser + '\'' +
                ", commentText='" + commentText + '\'' +
                '}';
    }
}
