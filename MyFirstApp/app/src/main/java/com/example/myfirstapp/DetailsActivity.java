package com.example.myfirstapp;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.lang.reflect.Type;

import com.example.myfirstapp.fragments.CardFragment;
import com.example.myfirstapp.fragments.CommentFragment;

import java.util.ArrayList;
import java.util.List;

import com.example.myfirstapp.interfaces.GetDataService;
import com.example.myfirstapp.models.Landmark;
import com.example.myfirstapp.network.RetrofitInstance;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsActivity extends AppCompatActivity {

    private LatLng mDestination;
    private EditText editText;
    private ArrayList<String> comments = new ArrayList<>();
    private String locationName;
    private ImageView likeImageView;
    private String landmarkID;
    private String landmarkImg;
    private GetLandmarksDetails details;
    private sendCommentTask sendComment;
    private TextView textView;
    private TextView descView;
    private ImageView imgView;
    private SetFavouriteTask favTask;
    private long usID;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_details);

        Intent intent =  getIntent();
        //locationName = intent.getStringExtra("location_name");
        landmarkID = intent.getStringExtra("location_id");
        landmarkImg = intent.getStringExtra("location_image");
        usID = intent.getLongExtra("userID", -1);
        // TO DO : GET LAT AND LONG FROM LANDMARK

        Double lat = intent.getDoubleExtra("lat", 45);
        Double lng = intent.getDoubleExtra("long", 19);

        mDestination = new LatLng(lat, lng);

      //  Log.i("selected location", locationName);
        textView = findViewById(R.id.detailsTitleView);
        descView = findViewById(R.id.detailsDescView);





     //   String locationName = intent.getStringExtra("location_name");
      //  Double lat = intent.getDoubleExtra("lat", 0);
      //  Double lng = intent.getDoubleExtra("long", 0);
      //  mDestination = new LatLng(lat, lng);
      //  Log.i("selected location", locationName);
        textView = findViewById(R.id.detailsTitleView);
        descView = findViewById(R.id.detailsDescView);
        imgView = findViewById(R.id.detailsCoverImageView);

      //  textView.setText(locationName);
       /* descView.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id feugiat turpis. " +

                "Curabitur sed erat interdum, blandit ex quis, posuere justo. Quisque fringilla eu libero non luctus. " +
                "Vestibulum porttitor ligula quis porta scelerisque. Duis purus justo, hendrerit ac lacinia quis, laoreet non elit. " +
                "Suspendisse et eleifend sapien. Sed viverra a enim vitae commodo. Vivamus quam velit, mollis sit amet ante vulputate, " +
                "ultricies elementum odio.");
*/
        details = new GetLandmarksDetails(Long.parseLong(landmarkID));
        details.execute((Void) null);

        // TO DO : proveriti da li je favourite ili nije

       boolean isFavourite = true;
        likeImageView = (ImageView)findViewById(R.id.likeImageView);
     /*   if(isFavourite){
            likeImageView.setTag(R.drawable.ic_favorite_full);
            likeImageView.setImageResource(R.drawable.ic_favorite_full);
        }
        else {
            likeImageView.setTag(R.drawable.ic_favorite_empty);
            likeImageView.setImageResource(R.drawable.ic_favorite_empty);
        }
*/
        likeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int id = (int)likeImageView.getTag();
                if( id == R.drawable.ic_favorite_empty){

                    likeImageView.setTag(R.drawable.ic_favorite_full);
                    likeImageView.setImageResource(R.drawable.ic_favorite_full);

                    favTask = new SetFavouriteTask(usID, Long.parseLong(landmarkID), true);
                    favTask.execute((Void) null);
                    Toast.makeText(DetailsActivity.this, " added to favourites",Toast.LENGTH_SHORT).show();
                }else{

                    likeImageView.setTag(R.drawable.ic_favorite_empty);
                    likeImageView.setImageResource(R.drawable.ic_favorite_empty);


                    favTask = new SetFavouriteTask(usID, Long.parseLong(landmarkID), false);
                    favTask.execute((Void) null);
                    Toast.makeText(DetailsActivity.this, locationName+" removed from favourites",Toast.LENGTH_SHORT).show();

                 // TO DO: REMOVE LANDMARK FROM FAVOURITES
                 //   ((MainActivity)getActivity()).removeFromFavourites(titleTextView.getText().toString());

                   // Toast.makeText(DetailsActivity.this, locationName+" removed from favourites",Toast.LENGTH_SHORT).show();



                }

            }
        });



     //   setupComments(true);


        addKeyListener();
    }

    public void addKeyListener() {

        // get edittext component
        editText = (EditText) findViewById(R.id.new_comment);

        // add a keylistener to keep track user input
        editText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                // if keydown and "enter" is pressed
                if ((event.getAction() == KeyEvent.ACTION_DOWN)
                        && (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    // display a floating message
                    comments.add(editText.getText().toString());

                    sendComment = new sendCommentTask(editText.getText().toString(), Long.parseLong(landmarkID));
                    //TODO: slanje komentara u bazu i dodavanje
                  /*  FragmentManager fm = getSupportFragmentManager();
                    for (Fragment fragment:getSupportFragmentManager().getFragments()) {
                        fm.beginTransaction().remove(fragment).commit();
                    }
                    setupComments(false);
*/
                    return true;

                }

                return false;
            }
        });
    }

    public void setupComments(boolean withDummy){
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);
        if(withDummy) {
            comments.add("Comment1");
            comments.add("Tesmporary text");
        }
        if (fragment == null) {
            fragment = new CommentFragment();
            Bundle args = new Bundle();

            args.putStringArrayList("wonders", comments);
            fragment.setArguments(args);

            fm.beginTransaction()
                    .add(R.id.commentFragmentContainer, fragment)
                    .commit();
        }

    }

    public void showDirections(View view) {
        Intent intent = new Intent(this,  MapsActivity.class);
        intent.putExtra("lat", mDestination.latitude);
        intent.putExtra("lng", mDestination.longitude);
        startActivity(intent);
    }


    public class GetLandmarksDetails extends AsyncTask<Void, Void, Boolean> {

        private long userID;
        private long landmarkID;



        public GetLandmarksDetails(long userID) {
            this.userID = userID;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            GetDataService service = RetrofitInstance.getRetrofitInstance().create(GetDataService.class);

            Call<List<CommentFragmentModel>> call = service.getLandDetails(userID);

            call.enqueue(new Callback<List<CommentFragmentModel>>() {
                @Override
                public void onResponse(Call<List<CommentFragmentModel>> call, Response<List<CommentFragmentModel>> response) {

                    if (response.isSuccessful()) {
                        if (response.code() == 200 && response.body().size() > 0) {


                            FragmentManager fm = getSupportFragmentManager();
                            Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);


                            Log.i("onResponse", "Hash map: get landmark"+ response.body().get(0).getLandmark());

                            Landmark landmark = response.body().get(0).getLandmark();
                            descView.setText(landmark.getDescription());
                            textView.setText(landmark.getName());
                            landmarkID = landmark.getId();

                            byte[] imgByte = Base64.decode(landmarkImg, Base64.DEFAULT);
                            if(imgByte!=null){
                                imgView.setImageBitmap(BitmapFactory.decodeByteArray(imgByte,0,imgByte.length));
                            }
                            Log.i("onResponseDetail", "Landmark name: "+ landmark.getName());


                            ArrayList<CommentFragmentModel> comm = (ArrayList<CommentFragmentModel>)response.body();

                            ArrayList<String> commText = new ArrayList<>();
                            ArrayList<String> commUser = new ArrayList<>();

                            Log.i("onResponseDetail", "Comment size:  "+comm.size());
                            for(int i = 0; i<comm.size();i++){
                                    commText.add(comm.get(i).commentText);
                                    commUser.add(comm.get(i).commentUser);
                                    Log.i("DetailsActivity","Comment text"+comm.get(i).commentText);
                            }
                            if (fragment == null) {
                                fragment = new CommentFragment();
                                Bundle args = new Bundle();

                                args.putStringArrayList("wonders", commText);
                                args.putStringArrayList("users", commUser);
                                fragment.setArguments(args);

                                fm.beginTransaction()
                                        .add(R.id.commentFragmentContainer, fragment)
                                        .commit();
                            }
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

                        //  mPasswordView.setError(getString(R.string.error_incorrect_password));
                        // mPasswordView.requestFocus();
                    }

                }


                @Override
                public void onFailure(Call<List<CommentFragmentModel>> call, Throwable t) {
                    Log.i("onFailure", "Usao u onFailure(DetailsActivity)");

                }
            });

            Call<Boolean> call2 = service.isFavourite(usID+";"+landmarkID);

            call2.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {

                    if (response.isSuccessful()) {
                        if (response.code() == 200) {
                            Log.i("BLA", "REsponse je " + response.body());
                            if(response.body()){
                                likeImageView.setTag(R.drawable.ic_favorite_full);
                                likeImageView.setImageResource(R.drawable.ic_favorite_full);
                            }
                                else {
                                likeImageView.setTag(R.drawable.ic_favorite_empty);
                                likeImageView.setImageResource(R.drawable.ic_favorite_empty);
                            }
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

                        //  mPasswordView.setError(getString(R.string.error_incorrect_password));
                        // mPasswordView.requestFocus();
                    }
                }


                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    Log.i("onFailure", "Usao u onFailure(DetailAcitivity)");

                }
            });

            return false;
        }

    }

    public class sendCommentTask extends AsyncTask<Void, Void, Boolean> {

        private long landmarkID;
        private String comment;



        public sendCommentTask(String comment, long landmarkID) {
            this.landmarkID = landmarkID;
            this.comment = comment;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            GetDataService service = RetrofitInstance.getRetrofitInstance().create(GetDataService.class);

            Call<Boolean> call = service.addComment(comment, landmarkID);

            call.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {

                    if (response.isSuccessful()) {
                        if (response.code() == 200) {
                     /*       FragmentManager fm = getSupportFragmentManager();
                            Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);


                            Landmark landmark = (Landmark) response.body().get("landmark");

                            descView.setText(landmark.getDescription());
                            textView.setText(landmark.getName());
                            Log.i("onResponseDetail", "Landmark name: "+ landmark.getName());

                            ArrayList<CommentFragmentModel> comm = (ArrayList<CommentFragmentModel>)response.body().get("comments");

                            ArrayList<String> commText = new ArrayList<>();
                            ArrayList<String> commUser = new ArrayList<>();

                            Log.i("onResponseDetail", "Comment size:  "+comm.size());
                            for(int i = 0; i<comm.size();i++){
                                commText.add(comm.get(i).commentText);
                                commUser.add(comm.get(i).commentUser);
                            }
                            if (fragment == null) {
                                fragment = new CommentFragment();
                                Bundle args = new Bundle();

                                args.putStringArrayList("wonders", commText);
                                args.putStringArrayList("users", commUser);
                                fragment.setArguments(args);

                                fm.beginTransaction()
                                        .add(R.id.commentFragmentContainer, fragment)
                                        .commit();
                            }*/
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

                        //  mPasswordView.setError(getString(R.string.error_incorrect_password));
                        // mPasswordView.requestFocus();
                    }
                }


                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    Log.i("onFailure", "Usao u onFailure(DetailAcitivity)");

                }
            });



            return false;
        }

    }

    public class SetFavouriteTask extends AsyncTask<Void, Void, Boolean> {

        private long userID;
        private long landmarkId;
        private boolean favourite;

        public SetFavouriteTask(long userID, long landmarkId, boolean favourite) {
            this.userID = userID;
            this.landmarkId = landmarkId;
            this.favourite = favourite;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            GetDataService service = RetrofitInstance.getRetrofitInstance().create(GetDataService.class);

            Call<Landmark> call;
            if(this.favourite == true){
                call = service.addToFavourite(this.userID+";"+this.landmarkId);
            }
            else {
                call = service.removeFromFavourite(this.userID+";"+this.landmarkId);
            }
            call.enqueue(new Callback<Landmark>() {
                @Override
                public void onResponse(Call<Landmark> call, Response<Landmark> response) {

                    if (response.isSuccessful()) {
                        if (response.code() == 200) {
                            Toast.makeText(getApplicationContext(), "Change applied", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                }


                @Override
                public void onFailure(Call<Landmark> call, Throwable t) {
                    Log.i("onFailure", "Usao u onFailure(MainActivity)");

                }
            });

            return false;
        }
    }
}
