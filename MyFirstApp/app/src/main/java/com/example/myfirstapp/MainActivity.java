package com.example.myfirstapp;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfirstapp.db.UserDBContentProvider;
import com.example.myfirstapp.db.UserSQLiteHelper;
import com.example.myfirstapp.interfaces.GetDataService;
import com.example.myfirstapp.models.Landmark;
import com.example.myfirstapp.models.User;
import com.example.myfirstapp.network.RetrofitInstance;

import com.example.myfirstapp.fragments.CardFragment;
import java.net.UnknownServiceException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    private DrawerLayout mDrawerLayout;
    private SearchView searchView;
    private String usEmail;
    private String usPassword;
    private String usUsername;
    private String usImage;
    private long usID;
    private static final int PROFILE_SET = 200;
    private static final int FAVES_OK = 500;
    private ImageView navImg;
    private ArrayList<String> Userfavourites = new ArrayList<String>();
    private GetLandmarksTask land;
    private SearchTask searchTask;
    private SetFavouriteTask favTask;

    public void addToFavourites(String wndr){
        Userfavourites.add(wndr);
    }
    public boolean isFavourite(String wndr) {return Userfavourites.indexOf(wndr)!=-1;}
    public void removeFromFavourites(String wndr){
        Userfavourites.remove(wndr);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        usEmail = intent.getStringExtra("email");
        usPassword = intent.getStringExtra("password");
        usUsername = intent.getStringExtra("username");
        usID = intent.getLongExtra("userId", -1);
        usImage = intent.getStringExtra("userImage");
        mDrawerLayout = findViewById(R.id.drawer_layout);
        searchView = (SearchView) findViewById(R.id.searchView);
        Button addLocButton = (Button) findViewById(R.id.button2);
        addLocButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addLoc();
            }
        });
        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        MenuItem it = navigationView.getMenu().getItem(0);
                        MenuItem settingsIt = navigationView.getMenu().getItem(1);
                        MenuItem favIt = navigationView.getMenu().getItem(2);
                        MenuItem UserLandmarksIt = navigationView.getMenu().getItem(3);
                        MenuItem logoutIt = navigationView.getMenu().getItem(4);
                        if (menuItem.equals(it)){
                           toProfile();
                        } else if (menuItem.equals(settingsIt)){
                            toSettings();

                        } else if (menuItem.equals(logoutIt)){
                            logoutUser();
                        } else if (menuItem.equals(UserLandmarksIt)) {
                            toUserLandmarks();
                        }
                        else if (menuItem.equals(favIt)){
                            toFavourites();
                        }
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        return true;
                    }
                });

        mDrawerLayout.addDrawerListener(
                new DrawerLayout.DrawerListener() {
                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        // Respond when the drawer's position changes
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        // Respond when the drawer is opened
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                        // Respond when the drawer is closed
                    }

                    @Override
                    public void onDrawerStateChanged(int newState) {
                        // Respond when the drawer motion state changes
                    }
                }
        );


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                searchTask = new SearchTask(query);
                searchTask.execute((Void) null);

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        land = new GetLandmarksTask(usID);
        land.execute((Void) null);
/*
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);

        if (fragment == null) {
            fragment = new CardFragment();
            Bundle args = new Bundle();
            String Wonders[] = {"Christ the Redeemer","Petra","Great Wall of China","Chichen Itza","Machu Picchu","Taj Mahal","Colosseum"};
            args.putStringArray("wonders", Wonders);
            fragment.setArguments(args);

            fm.beginTransaction()
                    .add(R.id.fragmentContainer, fragment)
                    .commit();
        }

        //setNavImage();
*/

    }

    public void startFavTask(long landmarkID, boolean favourite) {
        favTask = new SetFavouriteTask(usID, landmarkID, favourite);
        favTask.execute((Void) null);
    }

    public void toProfile(){
        Intent intent = new Intent(this,  ProfileActivity.class);
        intent.putExtra("email", usEmail);
        intent.putExtra("password", usPassword);
        intent.putExtra("username", usUsername);
        intent.putExtra("userId", usID);
        intent.putExtra("userImage", usImage);
        startActivityForResult(intent, PROFILE_SET);
        //startActivity(intent);
    }

    public void toSettings(){
        Intent intent = new Intent(this,  TravelimgPreferencesActivity.class);
        intent.putExtra("email", usEmail);
        intent.putExtra("password", usPassword);
        intent.putExtra("username", usUsername);
        intent.putExtra("userId", usID);
        intent.putExtra("userImage", usImage);
        startActivityForResult(intent, RESULT_OK);
    }

    public void toUserLandmarks(){
        Intent intent = new Intent(this,  UserLandmarksActivity.class);
        intent.putExtra("email", usEmail);
        intent.putExtra("password", usPassword);
        intent.putExtra("username", usUsername);
        intent.putExtra("userId", usID);
        intent.putExtra("userImage", usImage);
        intent.putStringArrayListExtra("favs", Userfavourites);
        startActivityForResult(intent, FAVES_OK);
        //startActivity(intent);
    }

    public void addLoc(){
        Intent intent = new Intent(this,  AddLocationActivity.class);
        intent.putExtra("email", usEmail);
        intent.putExtra("password", usPassword);
        intent.putExtra("username", usUsername);
        intent.putExtra("userId", usID);
        intent.putExtra("userImage", usImage);
        startActivity(intent);
    }

    private void toFavourites(){
        Intent intent = new Intent(this,  FavouritesActivity.class);
        intent.putExtra("email", usEmail);
        intent.putExtra("password", usPassword);
        intent.putExtra("username", usUsername);
        intent.putExtra("userId", usID);
        intent.putExtra("userImage", usImage);
        intent.putStringArrayListExtra("favs", Userfavourites);
        startActivityForResult(intent, FAVES_OK);
    }

    public void logoutUser(){
        Intent intent = new Intent(this,  LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            /*if (data.hasExtra("myData1")) {
                Toast.makeText(this, data.getExtras().getString("myData1"),
                        Toast.LENGTH_SHORT).show();
            }*/
            if(data.hasExtra("newLocName")){
                //TODO:ovde dodaj novu lokaciju u listu za cardFragmentView
            }
        } else if (resultCode == PROFILE_SET){
            usEmail = data.getStringExtra("email");
            usPassword = data.getStringExtra("password");
            usUsername = data.getStringExtra("username");
            usID = data.getLongExtra("userId", -1);
            usImage = data.getStringExtra("userImage");
            setNavImage();
        } else if (resultCode == FAVES_OK){
            usEmail = data.getStringExtra("email");
            usPassword = data.getStringExtra("password");
            usUsername = data.getStringExtra("username");
            usID = data.getLongExtra("userId", -1);
            setNavImage();
        }
    }

    public void setNavImage(){
    /*
        Uri queryUri = Uri.parse(UserDBContentProvider.CONTENT_URI_USERS + "/image/" + usEmail);
        String[] imageColumn = {UserSQLiteHelper.COLUMN_IMAGE};
        Cursor cursor = getContentResolver().query(queryUri, imageColumn, null, null,
                null);
        cursor.moveToFirst();
        byte[] imgByte = cursor.getBlob(0);
        cursor.close();
        if(imgByte!= null){
            navImg = (ImageView)findViewById(R.id.imageView1);
            navImg.setImageBitmap(BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length));
        }*/
        byte[] imgByte = Base64.decode(usImage, Base64.DEFAULT);
        if(imgByte!=null){
            navImg = (ImageView)findViewById(R.id.imageView1);
            navImg.setImageBitmap(BitmapFactory.decodeByteArray(imgByte,0,imgByte.length));
        }
    }
    /** Called when the user taps the Send button */
    public void sendMessage(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
       /* EditText editText = (EditText) findViewById(R.id.editText);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message); */
        startActivity(intent);
    }

    public void onDrawerToggle(View view) {
        TextView navUsername = (TextView) findViewById(R.id.nav_username);
        navUsername.setText(usUsername);
        setNavImage();
        mDrawerLayout.openDrawer(GravityCompat.START);
    }



    public class GetLandmarksTask extends AsyncTask<Void, Void, Boolean> {

        private long userID;

        public GetLandmarksTask(long userID){
            this.userID = userID;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            GetDataService service = RetrofitInstance.getRetrofitInstance().create(GetDataService.class);

            Call<List<Landmark>> call = service.getLandmarks(usID);

            call.enqueue(new Callback<List<Landmark>>() {
                @Override
                public void onResponse(Call<List<Landmark>> call, Response<List<Landmark>> response) {

                    if (response.isSuccessful())
                    {
                        if(response.code()==200) {
                            FragmentManager fm = getSupportFragmentManager();
                            Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);


                            ArrayList<Landmark> landmarks = (ArrayList<Landmark>)response.body();
                            ArrayList<String> titles = new ArrayList<String>();
                            ArrayList<String> images = new ArrayList<>();
                            ArrayList<String> ids = new ArrayList<>();
                            ArrayList<String> latitude = new ArrayList<>();
                            ArrayList<String> longitude = new ArrayList<>();
                            ArrayList<String> id = new ArrayList<>();
                            id.add(String.valueOf(usID));
                            Log.i("onResponse: ", "MainActivity(GetLandmarks) size"+landmarks.size());
                            for(int i = 0; i < landmarks.size(); i++){
                                titles.add(landmarks.get(i).getName());
                                images.add(landmarks.get(i).getPhoto());
                                ids.add(String.valueOf(landmarks.get(i).getId()));
                                latitude.add(String.valueOf(landmarks.get(i).getLatitude()));
                                longitude.add(String.valueOf(landmarks.get(i).getLongitude()));
                            }

                            if (fragment == null) {
                                fragment = new CardFragment();
                                Bundle args = new Bundle();

                                args.putStringArrayList("wonders", titles);
                                args.putStringArrayList("photos", images);
                                args.putStringArrayList("ids", ids);
                                args.putStringArrayList("latitude", latitude);
                                args.putStringArrayList("longitude", longitude);
                                args.putStringArrayList("userID", id);
                                fragment.setArguments(args);

                                fm.beginTransaction()
                                        .add(R.id.fragmentContainer, fragment)
                                        .commit();
                            }
                        }

                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

                        //  mPasswordView.setError(getString(R.string.error_incorrect_password));
                        // mPasswordView.requestFocus();
                    }
                }


                @Override
                public void onFailure(Call<List<Landmark>> call, Throwable t) {
                    Log.i("onFailure", "Usao u onFailure(CardFragment)");

                }
            });

            return false;
        }
    }

    public class SearchTask extends AsyncTask<Void, Void, Boolean> {

        private String search;

        public SearchTask(String search){
            this.search = search;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            GetDataService service = RetrofitInstance.getRetrofitInstance().create(GetDataService.class);

            Call<List<Landmark>> call = service.search(search);

            call.enqueue(new Callback<List<Landmark>>() {
                @Override
                public void onResponse(Call<List<Landmark>> call, Response<List<Landmark>> response) {

                    if (response.isSuccessful())
                    {
                        if(response.code()==200) {
                            FragmentManager fm = getSupportFragmentManager();
                            Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);


                            ArrayList<Landmark> landmarks = (ArrayList<Landmark>)response.body();
                            if(landmarks.size()==0){
                                Toast.makeText(getApplicationContext(), "No landmarks", Toast.LENGTH_SHORT).show();
                            }
                            ArrayList<String> titles = new ArrayList<String>();
                            ArrayList<String> images = new ArrayList<>();
                            ArrayList<String> ids = new ArrayList<>();
                            ArrayList<String> latitude = new ArrayList<>();
                            ArrayList<String> longitude = new ArrayList<>();
                            ArrayList<String> id = new ArrayList<>();
                            id.add(String.valueOf(usID));
                            Log.i("onResponse: ", "MainActivity(GetLandmarks) size"+landmarks.size());
                            for(int i = 0; i < landmarks.size(); i++){
                                titles.add(landmarks.get(i).getName());
                                images.add(landmarks.get(i).getPhoto());
                                ids.add(String.valueOf(landmarks.get(i).getId()));
                                latitude.add(String.valueOf(landmarks.get(i).getLatitude()));
                                longitude.add(String.valueOf(landmarks.get(i).getLongitude()));
                            }

                            if (fragment == null) {
                                fragment = new CardFragment();
                                Bundle args = new Bundle();

                                args.putStringArrayList("wonders", titles);
                                args.putStringArrayList("photos", images);
                                args.putStringArrayList("ids", ids);
                                args.putStringArrayList("latitude", latitude);
                                args.putStringArrayList("longitude", longitude);
                                args.putStringArrayList("userID", id);
                                fragment.setArguments(args);

                                fm.beginTransaction()
                                        .add(R.id.fragmentContainer, fragment)
                                        .commit();
                            }
                        }

                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

                        //  mPasswordView.setError(getString(R.string.error_incorrect_password));
                        // mPasswordView.requestFocus();
                    }
                }


                @Override
                public void onFailure(Call<List<Landmark>> call, Throwable t) {
                    Log.i("onFailure", "Usao u onFailure(CardFragment)");

                }
            });

            return false;
        }
    }

    public class SetFavouriteTask extends AsyncTask<Void, Void, Boolean> {

        private long userID;
        private long landmarkId;
        private boolean favourite;

        public SetFavouriteTask(long userID, long landmarkId, boolean favourite) {
            this.userID = userID;
            this.landmarkId = landmarkId;
            this.favourite = favourite;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            GetDataService service = RetrofitInstance.getRetrofitInstance().create(GetDataService.class);

            Call<Landmark> call;
            if(this.favourite == true){
                call = service.addToFavourite(this.userID+";"+this.landmarkId);
            }
            else {
                call = service.removeFromFavourite(this.userID+";"+this.landmarkId);
            }
            call.enqueue(new Callback<Landmark>() {
                @Override
                public void onResponse(Call<Landmark> call, Response<Landmark> response) {

                    if (response.isSuccessful()) {
                        if (response.code() == 200) {
                            Toast.makeText(getApplicationContext(), "Change applied", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                }


                @Override
                public void onFailure(Call<Landmark> call, Throwable t) {
                    Log.i("onFailure", "Usao u onFailure(MainActivity)");

                }
            });

            return false;
        }
    }
}
