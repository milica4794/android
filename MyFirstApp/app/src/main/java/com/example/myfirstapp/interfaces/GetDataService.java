package com.example.myfirstapp.interfaces;

import com.example.myfirstapp.CommentFragmentModel;
import com.example.myfirstapp.models.Landmark;
import com.example.myfirstapp.models.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface GetDataService {


    //USERS

    @POST("/login")
    Call<User> getUsers(@Body User user);

    @POST("/register")
    Call<User> addUser(@Body User user);

    @POST("/updateUser")
    Call<User> updateUser(@Body User user);

    //LANDMARKS

    @POST("/addLandmark")
    Call<Landmark> addLandmark(@Body HashMap<String, Object> landmark);

    @POST("/getFavourites")
    Call<List<Landmark>> getFavourites(@Body long id);

    @POST("/getLandmarks")
    Call<List<Landmark>> getLandmarks(@Body long userID);

    @POST("/getLandDetails")
    Call<List<CommentFragmentModel>> getLandDetails(@Body long userID);

    @POST("/addComment{id}")
    Call<Boolean> addComment(@Body String comment, @Path("id") long landmarkID);

    @POST("/getUserLandmarks")
    Call<List<Landmark>> getUserLandmarks(@Body long id);

    @POST("/search")
    Call<List<Landmark>> search(@Body String search);

    @POST("/addToFavourite/")
    Call<Landmark> addToFavourite(@Body String idUserLandmark);

    @POST("/removeFromFavourite")
    Call<Landmark> removeFromFavourite(@Body String idUserLandmark);

    @POST("/isFavourite")
    Call<Boolean> isFavourite(@Body String idUserLandmark);

}
