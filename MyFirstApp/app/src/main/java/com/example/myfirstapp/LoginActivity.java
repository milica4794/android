package com.example.myfirstapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfirstapp.db.UserDBContentProvider;
import com.example.myfirstapp.db.UserSQLiteHelper;
import com.example.myfirstapp.initialisers.DBUtil;
import com.example.myfirstapp.interfaces.GetDataService;
import com.example.myfirstapp.models.User;
import com.example.myfirstapp.network.ExecuteTask;
import com.example.myfirstapp.network.RetrofitInstance;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_CONTACTS;

public class LoginActivity extends AppCompatActivity {
        /**
         * Keep track of the login task to ensure we can cancel it if requested.
         */
        private UserLoginTask mAuthTask = null;

        // UI references.
        private AutoCompleteTextView mEmailView;
        private EditText mPasswordView;
        private View mProgressView;
        private View mLoginFormView;
        private ProgressDialog progressDialog;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login);
            //TODO:
            DBUtil.initDB(LoginActivity.this);
            // Set up the login form.
            mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

            mPasswordView = (EditText) findViewById(R.id.password);
            mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                    if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                        attemptLogin();
                        return true;
                    }
                    return false;
                }
            });


            Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
            mEmailSignInButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    attemptLogin();
                }
            });

            Button registerButton = (Button) findViewById(R.id.register_in_button);
            registerButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    goToRegister();
                }
            });

            mLoginFormView = findViewById(R.id.login_form);
            mProgressView = findViewById(R.id.login_progress);
        }

        private void goToRegister() {
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivityForResult(intent, RESULT_OK);
        }

        /**
         * Attempts to sign in or register the account specified by the login form.
         * If there are form errors (invalid email, missing fields, etc.), the
         * errors are presented and no actual login attempt is made.
         */
        private void attemptLogin() {
            if (mAuthTask != null) {
                return;
            }

            // Reset errors.
            mEmailView.setError(null);
            mPasswordView.setError(null);

            // Store values at the time of the login attempt.
            String email = mEmailView.getText().toString();
            String password = mPasswordView.getText().toString();

            boolean cancel = false;
            View focusView = null;

            // Check for a valid password, if the user entered one.
            if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                mPasswordView.setError(getString(R.string.error_invalid_password));
                focusView = mPasswordView;
                cancel = true;
            }

            // Check for a valid email address.
            if (TextUtils.isEmpty(email)) {
                mEmailView.setError(getString(R.string.error_field_required));
                focusView = mEmailView;
                cancel = true;
            } else if (!isEmailValid(email)) {
                mEmailView.setError(getString(R.string.error_invalid_email));
                focusView = mEmailView;
                cancel = true;
            }

            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.
                showProgress(true);
                mAuthTask = new UserLoginTask(email, password, this);
                mAuthTask.execute((Void) null);
            }
        }

        private boolean isEmailValid(String email) {
            //TODO: Replace this with your own logic
            return email.contains("@");
        }

        private boolean isPasswordValid(String password) {
            //TODO: Replace this with your own logic
            return password.length() >= 4;
        }

        /**
         * Shows the progress UI and hides the login form.
         */
        @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
        private void showProgress(final boolean show) {
            // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
            // for very easy animations. If available, use these APIs to fade-in
            // the progress spinner.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                        show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                    }
                });

                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                mProgressView.animate().setDuration(shortAnimTime).alpha(
                        show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                    }
                });
            } else {
                // The ViewPropertyAnimator APIs are not available, so simply show
                // and hide the relevant UI components.
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        }

        /**
         * Represents an asynchronous login/registration task used to authenticate
         * the user.
         */
        public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

            private final String mEmail;
            private final String mPassword;
            private final LoginActivity mOuterClass;
            private String mUsername;
            private long userID;
            private boolean exists;

            UserLoginTask(String email, String password, LoginActivity outerClass) {
                mEmail = email;
                mPassword = password;
                mOuterClass = outerClass;
            }
            
            @Override
            protected Boolean doInBackground(Void... params) {
                // TODO: attempt authentication against a network service.


                GetDataService service = RetrofitInstance.getRetrofitInstance().create(GetDataService.class);

                User user = new User();
                user.setEmail(mEmail);
                user.setPassword(mPassword);
                Call<User> call = service.getUsers(user);

                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        //   Log.i("onResponse", "Usao u onResponse(LoginActivity)");
                        //no user

                        if (response.isSuccessful()) {
                            if (response.code() == 200) {
                                Log.i("ResponseMessage", response.message().toLowerCase());
                                Log.i("UserLogin", "User with id " + response.body().getId() + "  has logged in!");
                                mUsername = response.body().getUsername();
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.putExtra("email", mEmail);
                                intent.putExtra("password", mPassword);
                                intent.putExtra("username", mUsername);
                                intent.putExtra("userId", response.body().getId());
                                intent.putExtra("userImage", response.body().getPicture());
                                startActivity(intent);
                                finish();
                            }

                        } else {
                            exists = false;
                            Toast.makeText(getApplicationContext(), "Wrong credentials", Toast.LENGTH_SHORT).show();
                            showProgress(false);
                            //  mPasswordView.setError(getString(R.string.error_incorrect_password));
                            // mPasswordView.requestFocus();
                        }
                    }


                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Log.i("onFailure", "Usao u onFailure(LoginActivity)");

                    }
                });

                return false;
/*
            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }



            Uri queryUri = Uri.parse(UserDBContentProvider.CONTENT_URI_USERS + "/email/" + mEmail);
            String[] allColumns = {UserSQLiteHelper.COLUMN_ID,
                    UserSQLiteHelper.COLUMN_USERNAME, UserSQLiteHelper.COLUMN_EMAIL, UserSQLiteHelper.COLUMN_PASSWORD};

            Cursor cursor = getContentResolver().query(queryUri, allColumns, null, null,
                    null);

            cursor.moveToFirst();
            String foundPass = cursor.getString(3);

            if (foundPass.equals(mPassword)) {
                mUsername = cursor.getString(1);
                userID = cursor.getLong(0);
                Log.i("nadjen user", mUsername);
                cursor.close();
                return true;
            }*/
            /*
            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mEmail)) {
                    // Account exists, return true if the password matches.
                    return pieces[1].equals(mPassword);
                }
            }*/

                // TODO: register the new account here.


            }

            @Override
            protected void onPostExecute(final Boolean success) {
                mAuthTask = null;
                //    showProgress(false);
/*
            if (success) {

                Intent intent = new Intent(this.mOuterClass, MainActivity.class);
                intent.putExtra("email", mEmail);
                intent.putExtra("password", mPassword);
                intent.putExtra("username", mUsername);
                intent.putExtra("userId", userID);
                startActivity(intent);
                finish();
            } else {
          //      mPasswordView.setError(getString(R.string.error_incorrect_password));
          //      mPasswordView.requestFocus();
            }*/
            }

            @Override
            protected void onCancelled() {
                mAuthTask = null;
                showProgress(false);
            }
        }
    }


