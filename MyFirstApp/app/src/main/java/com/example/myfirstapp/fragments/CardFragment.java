package com.example.myfirstapp.fragments;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfirstapp.DetailsActivity;
import com.example.myfirstapp.MainActivity;
import com.example.myfirstapp.MapsActivity;
import com.example.myfirstapp.R;
import com.example.myfirstapp.WonderModel;
import com.google.android.gms.maps.model.LatLng;


import java.util.ArrayList;



/*
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CardFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CardFragment extends Fragment {

    ArrayList<WonderModel> listitems = new ArrayList<>();
    RecyclerView MyRecyclerView;
    private ArrayList<String> wonders = new ArrayList<>();
    private ArrayList<String> photos = new ArrayList<>();
    private ArrayList<String> ids = new ArrayList<>();
    private ArrayList<String> latitude = new ArrayList<>();
    private ArrayList<String> longitude = new ArrayList<>();
    private Long usID;
    //String Wonders[] = {"Chichen Itza","Christ the Redeemer","Great Wall of China","Machu Picchu","Petra","Taj Mahal","Colosseum"};
    int  Images[] = {R.drawable.location_temp,R.drawable.location_temp,R.drawable.location_temp,R.drawable.location_temp,R.drawable.location_temp,
            R.drawable.location_temp,R.drawable.location_temp,};
            //{R.drawable.ic_person,R.drawable.ic_person,R.drawable.ic_person,R.drawable.ic_person,R.drawable.ic_person,R.drawable.ic_person,R.drawable.ic_person};
    private LatLng mDestination;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // TO DO: SET COORDINATES OF LANDMARK AS DESTINATION

      //  mDestination = new LatLng(45, 19);


        wonders = getArguments().getStringArrayList("wonders");
        photos = getArguments().getStringArrayList("photos");
        ids = getArguments().getStringArrayList("ids");
        latitude = getArguments().getStringArrayList("latitude");
        longitude = getArguments().getStringArrayList("longitude");
        usID = Long.parseLong(getArguments().getStringArrayList("userID").get(0));
        initializeList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_card, container, false);
        MyRecyclerView = (RecyclerView) view.findViewById(R.id.cardView);
        MyRecyclerView.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(getActivity());
        MyLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        if (listitems.size() > 0 & MyRecyclerView != null) {
            MyRecyclerView.setAdapter(new MyAdapter(listitems));
        }
        MyRecyclerView.setLayoutManager(MyLayoutManager);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public class MyAdapter extends RecyclerView.Adapter<MyViewHolder> {
        private ArrayList<WonderModel> list;

        public MyAdapter(ArrayList<WonderModel> Data) {
            list = Data;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
            // create a new view
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recycle_items, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {

            holder.titleTextView.setText(list.get(position).getCardName());
            holder.landmarkID = String.valueOf(list.get(position).getLandmarkID());
            holder.image = list.get(position).getImageResourceId();
            byte[] imgByte = Base64.decode(list.get(position).getImageResourceId(), Base64.DEFAULT);
            if(imgByte!=null){
                holder.coverImageView.setImageBitmap(BitmapFactory.decodeByteArray(imgByte,0,imgByte.length));
            }
            mDestination = new LatLng(Double.parseDouble(latitude.get(position)), Double.parseDouble(longitude.get(position)));
            holder.coverImageView.setTag(list.get(position).getImageResourceId());
         /*   boolean isFavourite = ((MainActivity)getActivity()).isFavourite(list.get(position).getCardName());
            if(isFavourite){
                holder.likeImageView.setTag(R.drawable.ic_favorite_full);
                holder.likeImageView.setImageResource(R.drawable.ic_favorite_full);
            }
            else {*/
                holder.likeImageView.setTag(R.drawable.ic_favorite_empty);
                holder.likeImageView.setImageResource(R.drawable.ic_favorite_empty);
       //     }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView titleTextView;
        public ImageView coverImageView;
        public ImageView likeImageView;
        public ImageView shareImageView;
        public String landmarkID;
        public String image;

        public MyViewHolder(View v) {
            super(v);
            titleTextView = (TextView) v.findViewById(R.id.titleTextView);
            coverImageView = (ImageView) v.findViewById(R.id.coverImageView);
            likeImageView = (ImageView) v.findViewById(R.id.likeImageView);
            shareImageView = (ImageView) v.findViewById(R.id.shareImageView);
            likeImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    int id = (int)likeImageView.getTag();
                    if( id == R.drawable.ic_favorite_empty){

                        likeImageView.setTag(R.drawable.ic_favorite_full);
                        likeImageView.setImageResource(R.drawable.ic_favorite_full);
                        ((MainActivity)getActivity()).startFavTask(Long.parseLong(landmarkID), true);

                        Toast.makeText(getActivity(),titleTextView.getText()+" added to favourites",Toast.LENGTH_SHORT).show();

                    }else{

                        likeImageView.setTag(R.drawable.ic_favorite_empty);
                        likeImageView.setImageResource(R.drawable.ic_favorite_empty);
                        ((MainActivity)getActivity()).startFavTask(Long.parseLong(landmarkID), false);
                        Toast.makeText(getActivity(),titleTextView.getText()+" removed from favourites",Toast.LENGTH_SHORT).show();


                    }

                }
            });



            shareImageView.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("ResourceType")
                @Override
                public void onClick(View v) {

                   /* Uri imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                            "://" + getResources().getResourcePackageName(coverImageView.getId())
                            + '/' + "drawable" + '/' + getResources().getResourceEntryName((int)coverImageView.getTag()));


                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.putExtra(Intent.EXTRA_STREAM,imageUri);
                    shareIntent.setType("image/jpeg");
                    startActivity(Intent.createChooser(shareIntent, getResources().getText(1)));*/
                    Intent intent = new Intent((MainActivity)getActivity(),  MapsActivity.class);
                    intent.putExtra("lat", mDestination.latitude);
                    intent.putExtra("lng", mDestination.longitude);
                    startActivity(intent);

                }
            });

            //TODO:detaljniji prikaz lokacije
            coverImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent detailsIntent = new Intent(getContext(), DetailsActivity.class);
                   // detailsIntent.putExtra("location_name", titleTextView.getText().toString());

                    detailsIntent.putExtra("userID", usID);
                    detailsIntent.putExtra("location_id", landmarkID);
                    detailsIntent.putExtra("location_image", image);
                    //TODO:da se posalje ID umesto naziva
                    //detailsIntent.putExtra(Intent.EXTRA_STREAM,imageUri);
                    startActivity(detailsIntent);

                }
            });

        }
    }

    public void initializeList() {
        listitems.clear();
        if(wonders!=null){
            for(int i =0;i<wonders.size();i++){


                WonderModel item = new WonderModel();
                item.setCardName(wonders.get(i));
                item.setImageResourceId(photos.get(i));
                item.setIsfav(0);
                item.setIsturned(0);
                item.setLandmarkID(ids.get(i));
                listitems.add(item);

             }

        }

    }


}
