package com.example.myfirstapp.fragments;

import android.support.v4.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfirstapp.CommentFragmentModel;
import com.example.myfirstapp.DetailsActivity;
import com.example.myfirstapp.MainActivity;
import com.example.myfirstapp.R;
import com.example.myfirstapp.WonderModel;

import java.util.ArrayList;

public class CommentFragment extends Fragment {
    ArrayList<CommentFragmentModel> listitems = new ArrayList<>();
    RecyclerView MyRecyclerView;
    private ArrayList<String> wonders = new ArrayList<>();
    private ArrayList<String> users = new ArrayList<>();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wonders = getArguments().getStringArrayList("wonders");
        users = getArguments().getStringArrayList("users");
        initializeList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.comment_recycle_items, container, false);
        MyRecyclerView = (RecyclerView) view.findViewById(R.id.comment_view);
        MyRecyclerView.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(getActivity());
        MyLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        if (listitems.size() > 0 & MyRecyclerView != null) {
            MyRecyclerView.setAdapter(new CommentFragment.MyAdapter(listitems));
        }
        MyRecyclerView.setLayoutManager(MyLayoutManager);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public class MyAdapter extends RecyclerView.Adapter<CommentFragment.MyViewHolder> {
        private ArrayList<CommentFragmentModel> list;

        public MyAdapter(ArrayList<CommentFragmentModel> Data) {
            list = Data;
        }

        @Override
        public CommentFragment.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.comment_item, parent, false);
            CommentFragment.MyViewHolder holder = new CommentFragment.MyViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(final CommentFragment.MyViewHolder holder, int position) {

            holder.commentUserView.setText(list.get(position).getCommentUser());
            holder.commentTextView.setText(list.get(position).getCommentText());


        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView commentUserView;
        public TextView commentTextView;


        public MyViewHolder(View v) {
            super(v);
            commentUserView = (TextView) v.findViewById(R.id.CommentUserView);
            commentTextView = (TextView) v.findViewById(R.id.CommentTextView);

        }
    }

    public void initializeList() {
        listitems.clear();
        if (wonders!= null) {
            for (int i = 0; i < wonders.size(); i++) {


                CommentFragmentModel item = new CommentFragmentModel();
                item.setCommentText(wonders.get(i));
                item.setCommentUser(users.get(i));

                listitems.add(item);
                Log.i("commentFragmetn", "Broj fragmenata"+item.toString());


            }
        }
    }
}
