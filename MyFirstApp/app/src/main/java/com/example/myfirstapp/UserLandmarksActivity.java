package com.example.myfirstapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.myfirstapp.fragments.FavouritesFragment;
import com.example.myfirstapp.interfaces.GetDataService;
import com.example.myfirstapp.models.Landmark;
import com.example.myfirstapp.network.RetrofitInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserLandmarksActivity extends AppCompatActivity {
    private String usEmail;
    private String usPassword;
    private String usUsername;
    private long usID;
    private static final int FAVES_OK = 500;
    private GetUserLandmarkTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);
        Intent intent = getIntent();
        usEmail = intent.getStringExtra("email");
        usPassword = intent.getStringExtra("password");
        usUsername = intent.getStringExtra("username");
        usID = intent.getLongExtra("userId", -1);

        task = new GetUserLandmarkTask(usID);
        task.execute((Void) null);
        
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent stuff = new Intent();
                stuff.putExtra("email", usEmail);
                stuff.putExtra("password", usPassword);
                stuff.putExtra("username", usUsername);
                stuff.putExtra("userId", usID);
                setResult(FAVES_OK, stuff);
                finish();
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {

        Intent stuff = new Intent();
        stuff.putExtra("email", usEmail);
        stuff.putExtra("password", usPassword);
        stuff.putExtra("username", usUsername);
        stuff.putExtra("userId", usID);
        setResult(FAVES_OK, stuff);
        super.onBackPressed();
    }

    public class GetUserLandmarkTask extends AsyncTask<Void,Void, Boolean> {

        private final long usId;


        public GetUserLandmarkTask(long userID) {
            this.usId = userID;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            GetDataService service = RetrofitInstance.getRetrofitInstance().create(GetDataService.class);
            Call<List<Landmark>> call = service.getUserLandmarks(usId);

            call.enqueue(new Callback<List<Landmark>>() {
                @Override
                public void onResponse(Call<List<Landmark>> call, Response<List<Landmark>> response) {
                       Log.i("onResponse", "Usao u onResponse(LoginActivity)");

                    if (response.isSuccessful())
                    {
                        if(response.code()==200) {
                            FragmentManager fm = getSupportFragmentManager();
                            Fragment fragment = fm.findFragmentById(R.id.favouritesFragmentContainer);


                            Log.i("ResponseMessage","Fetched user landmarks");
                            Log.i("ResponseBody",Integer.toString(response.body().size()));
                            ArrayList<Landmark> landmarks = (ArrayList<Landmark>)response.body();
                            ArrayList<String> title = new ArrayList<String>();
                            ArrayList<String> imgs = new ArrayList<>();
                            ArrayList<String> ids = new ArrayList<>();
                            for(int i = 0; i < landmarks.size(); i++){
                                title.add(landmarks.get(i).getName());
                                imgs.add(landmarks.get(i).getPhoto());
                                ids.add(Long.toString(landmarks.get(i).getId()));
                            }

                            if (fragment == null) {
                                fragment = new FavouritesFragment();
                                Bundle args = new Bundle();

                                args.putStringArrayList("user wonders", title);
                                args.putStringArrayList("images", imgs);
                                args.putString("userID", Long.toString(usID));
                                args.putStringArrayList("ids", ids);
                                fragment.setArguments(args);
                                ;
                                fm.beginTransaction()
                                        .add(R.id.favouritesFragmentContainer, fragment)
                                        .commit();
                            }
                        }

                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

                        //  mPasswordView.setError(getString(R.string.error_incorrect_password));
                        // mPasswordView.requestFocus();
                    }
                }


                @Override
                public void onFailure(Call<List<Landmark>> call, Throwable t) {
                    Log.i("onFailure", "Usao u onFailure(UserLandmarksActivity)");

                }
            });

            return false;
        }
    }
}
