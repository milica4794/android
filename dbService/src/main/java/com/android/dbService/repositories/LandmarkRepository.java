package com.android.dbService.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.android.dbService.models.Landmark;
import com.android.dbService.models.User;

@Repository
public interface LandmarkRepository extends CrudRepository<Landmark, Long> {


	ArrayList<Landmark> findByAddedBy(User user);
	List<Landmark> findByCityContaining(String city);
}
