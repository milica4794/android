package com.android.dbService.repositories;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.android.dbService.models.Has;
import com.android.dbService.models.Landmark;
import com.android.dbService.models.User;

@Repository
public interface HasRepository extends CrudRepository<Has, Long> {
	Has findByUserAndLandmark(User user, Landmark landmark);
}
