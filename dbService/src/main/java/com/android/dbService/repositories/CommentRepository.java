package com.android.dbService.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.android.dbService.models.Comment;
import com.android.dbService.models.Landmark;

@Repository	
public interface CommentRepository extends CrudRepository<Comment, Long> {

	List<Comment> findByComm(Landmark landmark);
}
