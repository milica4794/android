package com.android.dbService.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.android.dbService.models.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

	User findByEmailAndPassword(String email, String password);
	User findByUsername(String username);
}
