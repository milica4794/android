package com.android.dbService.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable = false)
	private String username;
	
	@Column(nullable = false)
	private String password;
	
	@Column(nullable = false)
	private String email;

	@Lob
	@Column(columnDefinition = "BLOB")
	private String picture;
	
	@Lob
	@Column(columnDefinition = "BLOB")
	private byte[] image;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="user")
	@JsonIgnore
	private Set<Has> has = new HashSet<Has>();
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="addedBy")
	@JsonIgnore
	private Set<Landmark> added = new HashSet<Landmark>();
	
	public User() {
		super();
	}

	
	public User(String username, String password, String email, String picture, byte[] image, Set<Has> has,
			Set<Landmark> added) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.picture = picture;
		this.image = image;
		this.has = has;
		this.added = added;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public byte[] getImage(){
		return image;
	}

	public void setImage(byte[] image){
		this.image = image;
	}

	public Set<Has> getHas() {
		return has;
	}

	public void setHas(Set<Has> has) {
		this.has = has;
	}

	public long getId() {
		return id;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public Set<Landmark> getAdded() {
		return added;
	}

	public void setAdded(Set<Landmark> added) {
		this.added = added;
	}

	
}
