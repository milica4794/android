package com.android.dbService.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Landmark implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column
	private String country;
	
	@Column
	private String city;
	
	@Column
	private double latitude; //Y
	
	@Column
	private double longitude; //X
	
	@Column
	private String description;
	
	@Column
	private String name;
	
	@ManyToOne()
	@JoinColumn(name="addedBy")
	private User addedBy;
	
	@Lob
	@Column(columnDefinition = "BLOB")
	private String photo;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="landmark")
	@JsonIgnore
	private Set<Has> has = new HashSet<Has>();
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="comm")
	@JsonIgnore
	private Set<Comment> comments = new HashSet<Comment>();

	public Landmark() {
		super();
	}

	public Landmark(String country, String city, double latitude, double longitude, String description, String name,
			User addedBy, String photo, Set<Has> has, Set<Comment> comments) {
		super();
		this.country = country;
		this.city = city;
		this.latitude = latitude;
		this.longitude = longitude;
		this.description = description;
		this.name = name;
		this.addedBy = addedBy;
		this.photo = photo;
		this.has = has;
		this.comments = comments;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(User addedBy) {
		this.addedBy = addedBy;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Set<Has> getHas() {
		return has;
	}

	public void setHas(Set<Has> has) {
		this.has = has;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Landmark [id=" + id + ", country=" + country + ", city=" + city + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", description=" + description + ", name=" + name + "]";
	}
	
	
	
}
