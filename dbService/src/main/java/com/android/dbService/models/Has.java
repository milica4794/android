package com.android.dbService.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Has implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column
	private boolean bookmarked;
	
	@ManyToOne()
	@JoinColumn(name="user")
	private User user;
	
	@ManyToOne()
	@JoinColumn(name="landmark")
	private Landmark landmark;

	public Has() {
		super();
	}

	public Has(boolean bookmarked, User user, Landmark landmark) {
		super();
		this.bookmarked = bookmarked;
		this.user = user;
		this.landmark = landmark;
	}

	public boolean isBookmarked() {
		return bookmarked;
	}

	public void setBookmarked(boolean bookmarked) {
		this.bookmarked = bookmarked;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Landmark getLandmark() {
		return landmark;
	}

	public void setLandmark(Landmark landmark) {
		this.landmark = landmark;
	}

	public long getId() {
		return id;
	}
	
	
	
}
