package com.android.dbService.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Comment implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable = false)
	private String commentText;
	
	@Column(nullable = false)
	private String commentUser;
	
	@ManyToOne()
	@JoinColumn(name="comm")
	private Landmark comm;

	public Comment() {
		super();
	}

	public Comment(String commentText, String commentUser, Landmark landmark) {
		super();
		this.commentText = commentUser;
		this.commentUser = commentUser;
		this.comm = landmark;
	}

	
	

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public String getCommentUser() {
		return commentUser;
	}

	public void setCommentUser(String commentUser) {
		this.commentUser = commentUser;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Landmark getLandmark() {
		return comm;
	}

	public void setLandmark(Landmark landmark) {
		this.comm = landmark;
	}

	public long getId() {
		return id;
	}
	
	
}
