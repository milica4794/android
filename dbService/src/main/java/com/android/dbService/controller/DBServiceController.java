package com.android.dbService.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.android.dbService.models.Comment;
import com.android.dbService.models.Has;
import com.android.dbService.models.Landmark;
import com.android.dbService.models.User;
import com.android.dbService.repositories.CommentRepository;
import com.android.dbService.repositories.HasRepository;
import com.android.dbService.repositories.LandmarkRepository;
import com.android.dbService.repositories.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class DBServiceController {

	@Autowired
	private CommentRepository commRepo;
	
	@Autowired
	private HasRepository hasRepo;
	
	@Autowired
	private LandmarkRepository landmarkRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@RequestMapping(value="/test/", method=RequestMethod.GET)
	public ResponseEntity<?> test(HttpSession session){
		System.out.println("TESTING JEN DVA TRI OPA");
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/login", method=RequestMethod.POST)
	public ResponseEntity<?> login(@RequestBody User user){
		System.out.println("User with email "+user.getEmail() +"and password"+user.getPassword());
		User exists = userRepo.findByEmailAndPassword(user.getEmail(), user.getPassword());
		if(exists==null)
			return new ResponseEntity<Void>(HttpStatus.EXPECTATION_FAILED);
		
		System.out.println("Niz fotke "+Arrays.toString(exists.getImage()));
		System.out.println("Found user with id  "+exists.getId());
		return new ResponseEntity<User>(exists,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> register(@RequestBody User user){
		System.out.println("Adding user with email: "+user.getEmail());

		User newUser = userRepo.save(user);
		
		return new ResponseEntity<User>(newUser,HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	public ResponseEntity<?> updateUser(@RequestBody User user){
		System.out.println("User change profile "+user.getId() +"username: "+user.getUsername());
		Optional<User> dbUser = userRepo.findById(user.getId());
		User DBUser = dbUser.get();
		DBUser.setEmail(user.getEmail());
		DBUser.setPicture(user.getPicture());
		DBUser.setPassword(user.getPassword());
		DBUser.setUsername(user.getUsername());
		userRepo.save(DBUser);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addLandmark", method = RequestMethod.POST)
	public ResponseEntity<?> addLandmark(@RequestBody HashMap<String, Object> landmark){
		ObjectMapper mapper = new ObjectMapper();
		Landmark landmark2 = mapper.convertValue(landmark.get("landmark"), Landmark.class);
		User temp = mapper.convertValue(landmark.get("user"), User.class);
		System.out.println("Adding landmark : "+landmark2.toString());
		System.out.println("Landmark for user : " + temp.getUsername());
		User dbUser = userRepo.findByUsername(temp.getUsername());
		landmark2.setAddedBy(dbUser);
		Landmark newLandmark = landmarkRepo.save(landmark2);
		Has h = new Has(true, dbUser, newLandmark);
		hasRepo.save(h);
		return new ResponseEntity<Landmark>(newLandmark, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/getFavourites", method = RequestMethod.POST)
	public ResponseEntity<?> getFavourites(@RequestBody String id){
		long id2 = Long.parseLong(id);
		Optional<User> dobavljen = userRepo.findById(id2);
		if(dobavljen.isPresent()){
			User user = dobavljen.get();
			System.out.println("Favourites for user : "+ user.getEmail());
			ArrayList<Landmark> favourites = new ArrayList<Landmark>();
			for(Has h : user.getHas()){
				Landmark t = h.getLandmark();
				Optional<Landmark> l = landmarkRepo.findById(t.getId());
				if(l.isPresent() && h.isBookmarked()){
					favourites.add(l.get());
				}
				System.out.println("Velicina bookmarka: " + favourites.size());
			}
			return new ResponseEntity<ArrayList<Landmark>>(favourites, HttpStatus.OK);
		}
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/addToFavourite", method = RequestMethod.POST)
	public ResponseEntity<?> addToFavourite(@RequestBody String idUserLandmark){
		String idUsera = idUserLandmark.split(";")[0];
		idUsera = idUsera.substring(1, idUsera.length());
		String idLandmarka = idUserLandmark.split(";")[1];
		idLandmarka = idLandmarka.substring(0, idLandmarka.length()-1);
		long idUsera2 = Long.parseLong(idUsera);
		long idLandmarka2 = Long.parseLong(idLandmarka);
		Optional<User> dobavljen = userRepo.findById(idUsera2);
		Optional<Landmark> dobavljenLandmark = landmarkRepo.findById(idLandmarka2);
		if(dobavljen.isPresent() && dobavljenLandmark.isPresent()){
			User user = dobavljen.get();
			Landmark landmark = dobavljenLandmark.get();
			System.out.println("Favourites for user : "+ user.getEmail());
			Has h = hasRepo.findByUserAndLandmark(user, landmark);
			if(h == null){
				h = new Has(true, user, landmark);
			}
			else {
				h.setBookmarked(true);
			}
			hasRepo.save(h);
			return new ResponseEntity<Landmark>(landmark, HttpStatus.OK);
		}
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(value = "/removeFromFavourite", method = RequestMethod.POST)
	public ResponseEntity<?> removeFromFavourite(@RequestBody String idUserLandmark){
		String idUsera = idUserLandmark.split(";")[0];
		idUsera = idUsera.substring(1, idUsera.length());
		String idLandmarka = idUserLandmark.split(";")[1];
		idLandmarka = idLandmarka.substring(0, idLandmarka.length()-1);
		long idUsera2 = Long.parseLong(idUsera);
		long idLandmarka2 = Long.parseLong(idLandmarka);
		Optional<User> dobavljen = userRepo.findById(idUsera2);
		Optional<Landmark> dobavljenLandmark = landmarkRepo.findById(idLandmarka2);
		if(dobavljen.isPresent() && dobavljenLandmark.isPresent()){
			User user = dobavljen.get();
			Landmark landmark = dobavljenLandmark.get();
			System.out.println("Not favourite for user : "+ user.getEmail());
			Has h = hasRepo.findByUserAndLandmark(user, landmark);
			if( h == null){
				h = new Has(false, user, landmark);
			}
			else {
				h.setBookmarked(false);
			}
			hasRepo.save(h);
			return new ResponseEntity<Landmark>(landmark, HttpStatus.OK);
		}
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/isFavourite", method = RequestMethod.POST)
	public ResponseEntity<?> isFavourite(@RequestBody String idUserLandmark){
		String idUsera = idUserLandmark.split(";")[0];
		idUsera = idUsera.substring(1, idUsera.length());
		String idLandmarka = idUserLandmark.split(";")[1];
		idLandmarka = idLandmarka.substring(0, idLandmarka.length()-1);
		long idUsera2 = Long.parseLong(idUsera);
		long idLandmarka2 = Long.parseLong(idLandmarka);
		Optional<User> dobavljen = userRepo.findById(idUsera2);
		Optional<Landmark> dobavljenLandmark = landmarkRepo.findById(idLandmarka2);
		if(dobavljen.isPresent() && dobavljenLandmark.isPresent()){
			User user = dobavljen.get();
			Landmark landmark = dobavljenLandmark.get();
			System.out.println("Is favourite for user : "+ user.getEmail());
			Has h = hasRepo.findByUserAndLandmark(user, landmark);
			if(h == null){
				return new ResponseEntity<Boolean>(false, HttpStatus.OK);
			}
			else if(h.isBookmarked()){
				new ResponseEntity<Boolean>(true, HttpStatus.OK);
			}
		}
		return new ResponseEntity<Boolean>(false, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getLandmarks", method = RequestMethod.POST)
	public ResponseEntity<?> getLandmarks(@RequestBody String id){
		List<Landmark> retLandmarks = new ArrayList<Landmark>();
		List<Landmark> landmarks = (List<Landmark>) landmarkRepo.findAll();
		long idUsera2 = Long.parseLong(id);
		Optional<User> dobavljen = userRepo.findById(idUsera2);
		if(dobavljen.isPresent()){
			User user = dobavljen.get();
			for(Landmark l : landmarks){
				Has h = hasRepo.findByUserAndLandmark(user, l);
				if(h != null && !h.isBookmarked())
					retLandmarks.add(l);
				else if (h==null){
					retLandmarks.add(l);
				}
			}
		}
		System.out.println("GET landmarks   "+retLandmarks.size());
		return new ResponseEntity<List<Landmark>>(retLandmarks, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getLandDetails", method = RequestMethod.POST)
	public ResponseEntity<?> getLandDetails(@RequestBody long id){
		HashMap<String, Object> landDetails = new HashMap<>();
		Optional<Landmark> opt = landmarkRepo.findById(id);
		Landmark landmark = null;
		if(opt.isPresent()) {
			landmark = opt.get();
		}
		
		List<Comment> comments = commRepo.findByComm(landmark);
		System.out.println(comments.size());
		if(comments.size() == 0){
			Comment c = new Comment();
			c.setCommentText("First comment");
			c.setCommentUser("Admin User");
			c.setLandmark(landmark);
			comments.add(c);
		}
		landDetails.put("comments", comments);
		landDetails.put("landmark", landmark);
		
		return new ResponseEntity<List<Comment>>(comments, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getLandmark", method = RequestMethod.POST)
	public ResponseEntity<?> getLandmark(@RequestBody long id){
		HashMap<String, Object> landDetails = new HashMap<>();
		Optional<Landmark> opt = landmarkRepo.findById(id);
		Landmark landmark = null;
		if(opt.isPresent()) {
			landmark = opt.get();
		}
		
		List<Comment> comments = commRepo.findByComm(landmark);
		
		
		landDetails.put("comments", comments);
		landDetails.put("landmark", landmark);
		
		return new ResponseEntity<Landmark>(landmark, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addComment{id}", method = RequestMethod.POST)
	public ResponseEntity<?> addComment(@PathVariable long id, @RequestBody String comment){
		System.out.println("Adding comment for landmark with id "+  id);
		Optional<Landmark> landmark = landmarkRepo.findById(id);
		if(landmark.isPresent()){
			Landmark lan = landmark.get();
			//Comment
		}
		
		
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getUserLandmarks", method = RequestMethod.POST)
	public ResponseEntity<?> getUserLandmarks(@RequestBody String id){
		long id2 = Long.parseLong(id);
		Optional<User> dobavljen = userRepo.findById(id2);
		if(dobavljen.isPresent()){
			User user = dobavljen.get();
			System.out.println("Landmarks for user : "+ user.getEmail());
			ArrayList<Landmark> landmarks = new ArrayList<Landmark>();
			landmarks = landmarkRepo.findByAddedBy(user);
			System.out.println("Velicina landmarka koje je napravio user je " + landmarks.size());
			return new ResponseEntity<ArrayList<Landmark>>(landmarks, HttpStatus.OK);
		}
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
	

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ResponseEntity<?> search(@RequestBody String query){
		
		List<Landmark> landmarks = landmarkRepo.findByCityContaining(query);
		System.out.println("Landmarks from city"+landmarks.size());
		return new ResponseEntity<List<Landmark>>(landmarks, HttpStatus.OK);
				
	}

}
